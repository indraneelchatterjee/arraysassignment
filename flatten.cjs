let ans = [];
let count = 0;

let flatten = (nestedArray, depth) => {
  if (!depth) {
    depth = 1;
  }
  for (let index = 0; index < nestedArray.length; index++) {
    if (Array.isArray(nestedArray[index]) && count < depth) {
      count++;
      flatten(nestedArray[index], depth);
      count--;
    } else if (nestedArray[index] !== undefined) {
      ans.push(nestedArray[index]);
    }
  }
  return ans;
};
module.exports = flatten;
