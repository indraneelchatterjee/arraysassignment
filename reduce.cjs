let reduce = (items, cb, startingVal) => {
  let acc = startingVal;
  let val = 0;
  if (startingVal === undefined) {
    acc = items[0];
    val = 1;
  }
  for (let index = val; index < items.length; index++) {
    acc = cb(acc, items[index], index, items);
  }
  return acc;
};

module.exports = reduce;
