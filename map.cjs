let map = (items, cb) => {
  let ans = [];
  for (let index = 0; index < items.length; index++) {
    ans.push(cb(items[index], index, items));
  }
  return ans;
};

module.exports = map;
