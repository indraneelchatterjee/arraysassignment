let filter = (items, cb) => {
  let ans = [];
  for (let index = 0; index < items.length; index++) {
    if (cb(items[index], index, items) === true) {
      ans.push(items[index]);
    }
  }
  return ans;
};

module.exports = filter;
